# korora-livecd-xfce.ks
#
# Description:
# - Korora Live Spin with the light-weight XFCE Desktop Environment


%packages

@networkmanager-submodules
@xfce-desktop
@xfce-apps
@xfce-extra-plugins
@xfce-media
@xfce-office

# unlock default keyring. FIXME: Should probably be done in comps
gnome-keyring-pam
# Admin tools are handy to have
@admin-tools
wget
# Handy for debugging
rfkill
# Better more popular browser
firefox
system-config-printer

# save some space
-autofs
-acpid
#-gimp-help
-desktop-backgrounds-basic
-xfce4-sensors-plugin

%end
