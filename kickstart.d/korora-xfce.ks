%include korora-live-xfce.ks
%include korora-base.ks
%include korora-common-packages.ks

#
# PACKAGES
#

%packages

# KORORA GNOME CONFIGURATION
korora-settings-xfce
korora-backgrounds-xfce
korora-backgrounds-extras-xfce


-yumex
yumex-dnf
-abiword
-abrt*
-audacious-plugins-amidi
-brasero
-brasero-nautilus
-claws-mail*
-dconf-editor
-deluge
-fedora-icon-theme
-geany
-gnome-abrt
-gnmeric
-im-chooser
-leafpad
-libreoffice-base
-mencoder
-midori
-mplayer
-parole
-pragha
#-ristretto
-smartmontools
-thunderbird
-totem*
transmission
-xfdashboard*
-xpdf
-xterm
@firefox
@libreoffice
@networkmanager-submodules
NetworkManager-adsl
NetworkManager-bluetooth
NetworkManager-iodine
NetworkManager-l2tp
NetworkManager-openconnect
NetworkManager-openswan
NetworkManager-openvpn
NetworkManager-pptp
NetworkManager-ssh
NetworkManager-vpnc
NetworkManager-wifi
NetworkManager-wwan
numlockx
alacarte
arc-theme
argyllcms
audacious
audacious-plugins*
bookworm
brltty
catfish
deja-dup
darktable
dconf-editor
#egtk-gtk2-theme
#egtk-gtk3-theme
ekiga
elementary-xfce-icon-theme
evince
f21-backgrounds-extras-xfce
#fbreader-gtk has been dropped from 29
gconf-editor
gnome-disk-utility
gnome-keyring-pam
gpgme
gtk-murrine-engine
gtk-unico-engine
gvfs-mtp
gvfs-smb
#gwibber # not available at the moment
hardlink
iok
libmatroska
libmpg123
libproxy-networkmanager
libsane-hpaio
liferea
#lightdm-gtk-greeter-settings
lirc
mousepad
mtools
ncftp
network-manager-applet
gst-transcoder
pitivi
pcsc-lite
pcsc-lite-ccid
#pharlap #no longer useful
pidgin
pidgin-guifications
pidgin-musictracker
pidgin-otr
pidgin-privacy-please
pidgin-sipe
pulseaudio-module-bluetooth
#purple-microblog - N/A - f22
#python3-lens-gtk
-python3-lens-qt
shotwell
simple-mtpfs
simple-scan
soundconverter
strongswan
system-config-language
system-config-users
#system-config-date - f26
system-config-printer-applet
thunderbird
tumbler-extras
webkitgtk4
wget
x264
xfce4-volumed
xfce4-whiskermenu-plugin
xfpanel-switch
xfsprogs
light-locker
slick-greeter
-xscreensaver-base
-xscreensaver-extras-base
-xscreensaver-gl-base
-xscreensaver-gl-extras
xvidcore
%end

%post

echo -e "\n*****\nPOST SECTION\n*****\n"

# KP - customise gtk-greeter
cat > /etc/lightdm/slick-greeter.conf << EOF
[Greeter]
background=/usr/share/backgrounds/korora/default/standard/korora.png
draw-grid=false
theme-name=Arc-Dark
show-power=true
show-keyboard=false
activate-numlock=true
EOF

cat > /.buildstamp << EOF
[Main]
Product=Korora XFCE
IsFinal=True
EOF

cat >> /etc/rc.d/init.d/livesys << EOF
setsebool -P rsync_full_access 1
# KP - re'sync the /etc/skel settings for xfce and 
rsync -Pa /etc/skel/.config/xfce4 /home/liveuser/.config

# make sure to set the right permissions and selinux contexts
chown -R liveuser:liveuser /home/liveuser/
# Prevent SELinux Complaining about rsync access
restorecon -R /home/liveuser/
EOF

%end
